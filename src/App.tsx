import React,{useState,useEffect} from 'react';
import './App.css';
import Map from './Map';
import { loadMapApi } from './Utils/GoogleMapsUtils';

function App() {
  const [scriptLoaded, setScriptLoaded] = useState(false);

  useEffect(() => {
    const googleMapScript = loadMapApi();
    googleMapScript.addEventListener( 'load', function(){
      console.log('script getting loaded');
      setScriptLoaded(true);
    })
  }, []);

  return (
    <div className="App">
      <header>Map typescript example</header>
        {scriptLoaded && (
          <Map
            mapType = {google.maps.MapTypeId.ROADMAP}
            mapTypeControl = {true}
          />
        )}
    </div>
  );
}

export default App;
