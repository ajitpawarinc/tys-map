import React,{useRef, useEffect, useState} from 'react';
import './Map.scss';
interface IMap{
    mapType: google.maps.MapTypeId,
    mapTypeControl?:boolean;
}

interface IMarker{
    address:string;
    latitude:number;
    longitude:number;
}

type GoogleLatLang = google.maps.LatLng;
type GoogleMap = google.maps.Map;
type GoogleMarker = google.maps.Marker;

const Map: React.FC<IMap> = ({mapType, mapTypeControl=false })=>{
    const ref = useRef<HTMLDivElement>(null);
    const [map, setMap] = useState<GoogleMap>();
    const [marker, setMarker] = useState<IMarker>();

    const startMap = ():void =>{
        if( !map ){
            defaultMapStart();
        }
    }

    const defaultMapStart = ():void => {
        const defaultAddress = new google.maps.LatLng( 18.52, 73.85 );
        initMap( 7, defaultAddress );
    };
    const initEventListener = ():void =>{
        if( map ){
            google.maps.event.addListener( map, 'click', function(e){
                coordinateToAddress( e.latLng );
            })
        }

    };

    const coordinateToAddress = async ( coordinate:GoogleLatLang  ) => {
        const geocoder = new google.maps.Geocoder();
        await geocoder.geocode({ location:coordinate}, function( results, status ){
            if( status === 'OK' ){
                console.log(  );
                setMarker({
                    address:results[0].formatted_address,
                    latitude:coordinate.lat(),
                    longitude:coordinate.lng()
                });
            } else {
                //todo exception
            }
        });
    }

    const addSingleMarker = () :void => {
        if( marker ){
            addMarker( new google.maps.LatLng( marker.latitude, marker.longitude ) );
        }
    };

    useEffect( addSingleMarker, [marker]);

    const addMarker = ( location:GoogleLatLang ) :void => {
        const marker:GoogleMarker = new google.maps.Marker( {
            position:location,
            map:map,
            icon: getIconAttrs( '#000000' )
        });
    };

    const getIconAttrs = ( iconColor:string ) => {
        return {
            path:google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
            fillColor: iconColor,
            fillOpacity:0.8,
            strokeColor: 'pink',
            strokeWeight:2,
            scale: 10,
        };
    }

    useEffect( initEventListener, [map]);

    const initMap = ( zoomLevel: number, address: GoogleLatLang ):void => {
        if( ref.current ){
            setMap(
                new google.maps.Map( ref.current, {
                    zoom: zoomLevel,
                    center: address,
                    mapTypeControl: mapTypeControl,
                    streetViewControl:false,
                    rotateControl:false,
                    scaleControl:true,
                    fullscreenControl:false,
                    panControl:false,
                    zoomControl:true,
                    gestureHandling:'cooperative',
                    mapTypeId: mapType,
                    draggableCursor:'pointer'
                })
            );
        }
      
    };
    
    useEffect(() => {
        startMap();
    }, [map]);

    return (
        <div className="map-container">
            <div ref={ref} className="map-container__map"></div>
        </div>
    )

};

export default Map;