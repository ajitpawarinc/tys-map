export const loadMapApi = () => {
    const mapsUrl = `https://maps.googleapis.com/maps/api/js?key=AIzaSyAiR1hQuNTZajTx1giQVlRwVeWj0a-9_rQ&libraries=places&v=quarterly`;
    const scripts = document.getElementsByTagName('script');

    for( let i=0; i<scripts.length; i++){
        if(scripts[i].src.indexOf(mapsUrl) === 0){
            return scripts[i];
        }
    }
    console.log('creating googlemap script');

    const googleMapScript = document.createElement( 'script' );
    googleMapScript.src = mapsUrl;
    googleMapScript.async = true;
    googleMapScript.defer = true;
    window.document.body.appendChild( googleMapScript );

    return googleMapScript;
}